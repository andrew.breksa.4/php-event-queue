<?php declare(strict_types=1);


namespace EventQueue\Test\Unit;


use EventQueue\DeferredPublisher;
use EventQueue\PublisherInterface;
use Mockery;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidFactoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class MemoryQueuePublisherTest
 * @package EventQueue\Test\Unit
 * @author Andrew Breksa <andrew@andrewbreksa.com>
 */
class DeferredPublisherTest extends TestCase
{

    public function testQueue()
    {
        $publisher = Mockery::mock(PublisherInterface::class);
        $uuidFactory = Mockery::mock(UuidFactoryInterface::class);
        $queue = new DeferredPublisher(
            $publisher,
            $uuidFactory
        );

        $uuid = Mockery::mock(UuidInterface::class);
        $uuid->shouldReceive('toString')
            ->andReturn('uuid_1', 'uuid_2')
            ->twice();

        $uuidFactory->shouldReceive('uuid4')
            ->andReturn($uuid, $uuid)
            ->twice();

        $publisher->shouldReceive('publish')
            ->with('low', 'test_event', ['foo' => 'bar'], 'uuid_1')
            ->andReturnSelf()
            ->once();

        $publisher->shouldReceive('publish')
            ->with('low2', 'test_event', ['foo' => 'bar'], 'uuid_2')
            ->andReturnSelf()
            ->once();

        $this->assertEquals('uuid_1', $queue->publish('low', 'test_event', ['foo' => 'bar']));
        $this->assertEquals('uuid_2', $queue->publish('low2', 'test_event', ['foo' => 'bar']));

        $queue->flush();

        Mockery::close();
    }

}
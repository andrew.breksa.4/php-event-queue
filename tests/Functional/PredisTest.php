<?php declare(strict_types=1);


namespace EventQueue\Test\Functional;


use EventQueue\ConsumerInterface;
use EventQueue\Consumers\PredisConsumer;
use EventQueue\Encoders\JsonMessageEncoder;
use EventQueue\Exceptions\QueueDoesNotExistException;
use EventQueue\Message;
use EventQueue\MessageEncoderInterface;
use EventQueue\MessageHandlerInterface;
use EventQueue\Publishers\PredisPublisher;
use Predis\Client;
use Predis\ClientInterface;
use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\UuidFactoryInterface;

/**
 * Class PredisConsumerTest
 * @package EventQueue\Test\Functional
 * @author Andrew Breksa <andrew@andrewbreksa.com>
 */
class PredisTest extends FunctionalTestBase
{

    /**
     * @var ClientInterface
     */
    protected $predis;

    /**
     * @var MessageEncoderInterface
     */
    protected $encoder;

    /**
     * @var UuidFactoryInterface
     */
    protected $uuidFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->predis = new Client([
            'host' => getenv('REDIS_HOST'),
            'port' => getenv('REDIS_PORT')
        ]);
        $this->encoder = new JsonMessageEncoder();
        $this->uuidFactory = new UuidFactory();
    }

    public function testConsumeTrue()
    {
        $publisher = new PredisPublisher(
            $this->predis,
            $this->encoder,
            $this->uuidFactory
        );

        $publisher->registerQueueKeyMapping('low', 'low_queue');

        $publisher->publish('low', 'test_event', [
            'foo' => 'bar'
        ]);

        $consumer = new PredisConsumer(
            $this->predis,
            $this->encoder
        );

        $consumer->registerQueueKeyMapping('low', 'low_queue');

        $handler = new class($consumer) implements MessageHandlerInterface {

            /**
             * @var ConsumerInterface
             */
            protected $consumer;

            /**
             *  constructor.
             * @param ConsumerInterface $consumer
             */
            public function __construct(ConsumerInterface $consumer)
            {
                $this->consumer = $consumer;
            }


            public function handle(Message $message): bool
            {
                $this->consumer->ack($message);
                return true;
            }
        };

        $this->assertTrue($consumer->consume('low', $handler));
    }

    public function testConsumeFalse()
    {
        $publisher = new PredisPublisher(
            $this->predis,
            $this->encoder,
            $this->uuidFactory
        );

        $publisher->registerQueueKeyMapping('low', 'low_queue');

        $publisher->publish('low', 'test_event', [
            'foo' => 'bar'
        ]);

        $consumer = new PredisConsumer(
            $this->predis,
            $this->encoder
        );

        $consumer->registerQueueKeyMapping('low', 'low_queue');

        $handler = new class implements MessageHandlerInterface {
            public function handle(Message $message): bool
            {
                return false;
            }
        };

        $this->assertFalse($consumer->consume('low', $handler));
    }

    public function testConsumeNull()
    {

        $consumer = new PredisConsumer(
            $this->predis,
            $this->encoder
        );

        $consumer->registerQueueKeyMapping('low', 'low_queue');

        $handler = new class implements MessageHandlerInterface {
            public function handle(Message $message): bool
            {
                return true;
            }
        };

        $this->assertNull($consumer->consume('low', $handler));
    }


    public function testConsumeNoQueue()
    {

        $consumer = new PredisConsumer(
            $this->predis,
            $this->encoder
        );

        $handler = new class implements MessageHandlerInterface {
            public function handle(Message $message): bool
            {
                return true;
            }
        };

        $this->expectException(QueueDoesNotExistException::class);
        $this->assertNull($consumer->consume('low', $handler));
    }

    public function testPublishNoQueue()
    {
        $publisher = new PredisPublisher(
            $this->predis,
            $this->encoder,
            $this->uuidFactory
        );

        $this->expectException(QueueDoesNotExistException::class);
        $publisher->publish('low', 'test_event', [
            'foo' => 'bar'
        ]);
    }

    public function testPublish()
    {
        $publisher = new PredisPublisher(
            $this->predis,
            $this->encoder,
            $this->uuidFactory
        );

        $publisher->registerQueueKeyMapping('low', 'low_queue');

        $publisher->publish('low', 'test_event', [
            'foo' => 'bar'
        ]);

        $this->assertEquals(1, $this->predis->exists('low_queue'));

        $messageString = $this->predis->lpop('low_queue');
        $message = $this->encoder->decode($messageString);
        $this->assertEquals('test_event', $message->getEvent());
        $this->assertEquals(['foo' => 'bar'], $message->getData());
        $this->assertNotNull($message->getUuid());
    }


}
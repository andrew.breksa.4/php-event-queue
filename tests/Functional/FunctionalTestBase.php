<?php declare(strict_types=1);


namespace EventQueue\Test\Functional;


use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

/**
 * Class FunctionalTestBase
 * @package EventQueue\Test\Functional
 * @author Andrew Breksa <andrew@andrewbreksa.com>
 */
class FunctionalTestBase extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        if (file_exists(__DIR__ . '/../../.env')) {
            $dotEnv = Dotenv::createUnsafeImmutable(__DIR__ . '/../../');
            $dotEnv->load();
        }
    }

}
<?php declare(strict_types=1);


namespace EventQueue\Test\Functional;


use Aws\Credentials\CredentialProvider;
use Aws\Sqs\SqsClient;
use EventQueue\ConsumerInterface;
use EventQueue\Consumers\SqsConsumer;
use EventQueue\Encoders\JsonMessageEncoder;
use EventQueue\Exceptions\QueueDoesNotExistException;
use EventQueue\Message;
use EventQueue\MessageEncoderInterface;
use EventQueue\MessageHandlerInterface;
use EventQueue\Publishers\SqsPublisher;
use Mockery;
use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\UuidFactoryInterface;

/**
 * Class SqsTest
 * @package EventQueue\Test\Functional
 * @author Andrew Breksa <andrew@andrewbreksa.com>
 */
class SqsTest extends FunctionalTestBase
{

    /**
     * @var SqsClient
     */
    protected $client;

    /**
     * @var MessageEncoderInterface
     */
    protected $encoder;

    /**
     * @var UuidFactoryInterface
     */
    protected $uuidFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = new SqsClient([
            'region' => 'us-west-2',
            'version' => '2012-11-05',
            'credentials' => CredentialProvider::env(),
            'endpoint' => getenv('SQS_ENDPOINT'),
        ]);
        $this->encoder = new JsonMessageEncoder();
        $this->uuidFactory = new UuidFactory();
        $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');

        $this->client->purgeQueue(['QueueUrl' => $queueUrl]);
    }

    public function tearDown(): void
    {
        $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');

        $this->client->purgeQueue(['QueueUrl' => $queueUrl]);
        parent::tearDown();
    }

    public function testPublish()
    {
        $publisher = new SqsPublisher(
            $this->client,
            $this->encoder,
            $this->uuidFactory
        );

        $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');

        $publisher->registerQueueKeyMapping('low', $queueUrl);

        $publisher->publish('low', 'test_event', [
            'foo' => 'bar'
        ]);

        $consumer = new SqsConsumer(
            $this->client,
            $this->encoder
        );

        $consumer->registerQueueKeyMapping('low', $queueUrl);

        $handler = Mockery::mock(MessageHandlerInterface::class);

        $handler->shouldReceive('handle')
            ->withArgs(function (Message $message) {

                $this->assertArrayHasKey('queueUrl', $message->getAttributes());
                $this->assertArrayHasKey('receiptHandle', $message->getAttributes());
                $this->assertNotNull($message->getAttributes()['receiptHandle']);
                $this->assertEquals('test_event', $message->getEvent());
                $this->assertEquals(['foo' => 'bar'], $message->getData());

                return true;
            })
            ->andReturn(true)
            ->once();

        $consumer->consume('low', $handler);


        Mockery::close();
    }

    public function testPublishBadQueue()
    {
        $publisher = new SqsPublisher(
            $this->client,
            $this->encoder,
            $this->uuidFactory
        );

        $this->expectException(QueueDoesNotExistException::class);
        $publisher->publish('low', 'test_event', [
            'foo' => 'bar'
        ]);

    }

    public function testConsumeNoQueue()
    {
        $publisher = new SqsConsumer(
            $this->client,
            $this->encoder
        );

        $this->expectException(QueueDoesNotExistException::class);
        $publisher->consume('low', Mockery::mock(MessageHandlerInterface::class));

        Mockery::close();
    }

    public function testNoMessages()
    {
        $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');

        $this->client->purgeQueue(['QueueUrl' => $queueUrl]);
        sleep(5);

        $publisher = new SqsPublisher(
            $this->client,
            $this->encoder,
            $this->uuidFactory
        );

        $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');

        $publisher->registerQueueKeyMapping('low', $queueUrl);

        $consumer = new SqsConsumer(
            $this->client,
            $this->encoder
        );

        $consumer->registerQueueKeyMapping('low', $queueUrl);

        $handler = Mockery::mock(MessageHandlerInterface::class);
        $handler->shouldReceive('handle')
            ->never();

        $this->assertNull($consumer->consume('low', $handler));

        Mockery::close();
    }

    public function testMessageAck()
    {

        $publisher = new SqsPublisher(
            $this->client,
            $this->encoder,
            $this->uuidFactory
        );

        $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');

        $publisher->registerQueueKeyMapping('low', $queueUrl);

        $publisher->publish('low', 'test_event', [
            'foo' => 'bar'
        ]);

        $consumer = new SqsConsumer(
            $this->client,
            $this->encoder
        );

        $consumer->registerQueueKeyMapping('low', $queueUrl);

        $handler = new class($consumer) implements MessageHandlerInterface {

            /**
             * @var ConsumerInterface
             */
            protected $consumer;

            /**
             *  constructor.
             * @param ConsumerInterface $consumer
             */
            public function __construct(ConsumerInterface $consumer)
            {
                $this->consumer = $consumer;
            }


            public function handle(Message $message): bool
            {
                $this->consumer->ack($message);

                return true;
            }
        };

        $this->assertTrue($consumer->consume('low', $handler, 10));
    }

}
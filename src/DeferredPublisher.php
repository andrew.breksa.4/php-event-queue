<?php declare(strict_types=1);


namespace EventQueue;


use Ramsey\Uuid\UuidFactoryInterface;
use SplQueue;

/**
 * Class BatchPublisher
 *
 * @package EventQueue
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class DeferredPublisher implements PublisherInterface
{

    /**
     * @var SplQueue
     */
    protected $queue;

    /**
     * @var PublisherInterface
     */
    protected $publisher;

    /**
     * @var UuidFactoryInterface
     */
    protected $uuidFactory;

    /**
     * MemoryQueuePublisher constructor.
     *
     * @param PublisherInterface $publisher
     */
    public function __construct(PublisherInterface $publisher, UuidFactoryInterface $uuidFactory)
    {
        $this->publisher = $publisher;
        $this->uuidFactory = $uuidFactory;
        $this->queue = new SplQueue();
    }

    /**
     * @param string $queue
     * @param string $eventName
     * @param array $data
     * @param string|null $uuid
     * @return string
     */
    public function publish(string $queue, string $eventName, array $data, string $uuid = null): string
    {
        if ($uuid === null) {
            $uuid = $this->uuidFactory->uuid4()->toString();
        }
        $this->queue->enqueue([$queue, $eventName, $data, $uuid]);
        return $uuid;
    }

    public function flush(): void
    {
        foreach ($this->queue as $item) {
            $this->publisher->publish($item[0], $item[1], $item[2], $item[3]);
        }
    }
}
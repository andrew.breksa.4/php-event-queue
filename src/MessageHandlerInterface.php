<?php declare(strict_types=1);


namespace EventQueue;

/**
 * Interface MessageHandlerInterface
 *
 * @package EventQueue
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
interface MessageHandlerInterface
{

    /**
     * @param  Message $message
     * @return bool
     */
    public function handle(Message $message): bool;

}
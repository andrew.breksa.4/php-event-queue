<?php declare(strict_types=1);


namespace EventQueue\Publishers;


use EventQueue\Exceptions\QueueDoesNotExistException;
use EventQueue\Message;
use EventQueue\MessageEncoderInterface;
use EventQueue\PublisherInterface;
use Predis\ClientInterface;
use Ramsey\Uuid\UuidFactory;

/**
 * Class PredisPublisher
 *
 * @package EventQueue\Publishers
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class PredisPublisher implements PublisherInterface
{

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var array<string, string>
     */
    protected $queueKeyMapping = [];

    /**
     * @var UuidFactory
     */
    protected $uuidFactory;

    /**
     * @var MessageEncoderInterface
     */
    protected $messageEncoder;

    /**
     * PredisPublisher constructor.
     *
     * @param ClientInterface         $client
     * @param UuidFactory             $uuidFactory
     * @param MessageEncoderInterface $messageEncoder
     * @param array<string, string>   $queueKeyMapping
     */
    public function __construct(
        ClientInterface $client,
        MessageEncoderInterface $messageEncoder,
        UuidFactory $uuidFactory,
        array $queueKeyMapping = []
    ) {
        $this->client = $client;
        $this->queueKeyMapping = $queueKeyMapping;
        $this->uuidFactory = $uuidFactory;
        $this->messageEncoder = $messageEncoder;
    }

    /**
     * @param  string $queue
     * @param  string $key
     * @return $this
     */
    public function registerQueueKeyMapping(string $queue, string $key): PredisPublisher
    {
        $this->queueKeyMapping[$queue] = $key;
        return $this;
    }

    /**
     * @param  string $queue
     * @param  string $eventName
     * @param  array  $data
     * @return string
     */
    public function publish(string $queue, string $eventName, array $data, string $uuid = null): string
    {
        if (!array_key_exists($queue, $this->queueKeyMapping)) {
            throw new QueueDoesNotExistException('The queue mapping for ' . $queue . ' does not exist');
        }

        $key = $this->queueKeyMapping[$queue];

        if ($uuid === null) {
            $uuid = $this->uuidFactory->uuid4()->toString();
        }

        $this->client->lpush(
            $key, [
                $this->messageEncoder->encode(new Message($uuid, $eventName, $data))
            ]
        );

        return $uuid;
    }
}
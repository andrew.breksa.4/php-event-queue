<?php declare(strict_types=1);


namespace EventQueue\Publishers;


use Aws\Sqs\SqsClient;
use EventQueue\Exceptions\QueueDoesNotExistException;
use EventQueue\Message;
use EventQueue\MessageEncoderInterface;
use EventQueue\PublisherInterface;
use Ramsey\Uuid\UuidFactoryInterface;

/**
 * Class SqsPublisher
 *
 * @package EventQueue\Publishers
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class SqsPublisher implements PublisherInterface
{

    /**
     * @var SqsClient
     */
    protected $client;

    /**
     * @var array<string, string>
     */
    protected $queueUrlMapping = [];

    /**
     * @var MessageEncoderInterface
     */
    protected $messageEncoder;

    /**
     * @var UuidFactoryInterface
     */
    protected $uuidFactory;

    /**
     * SqsConsumer constructor.
     *
     * @param SqsClient               $client
     * @param array<string, string>   $queueUrlMapping
     * @param MessageEncoderInterface $messageEncoder
     */
    public function __construct(
        SqsClient $client,
        MessageEncoderInterface $messageEncoder,
        UuidFactoryInterface $uuidFactory,
        array $queueUrlMapping = []
    ) {
        $this->client = $client;
        $this->queueUrlMapping = $queueUrlMapping;
        $this->messageEncoder = $messageEncoder;
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * @param  string $queue
     * @param  string $url
     * @return $this
     */
    public function registerQueueKeyMapping(string $queue, string $url): SqsPublisher
    {
        $this->queueUrlMapping[$queue] = $url;
        return $this;
    }

    /**
     * @param  string      $queue
     * @param  string      $eventName
     * @param  array       $data
     * @param  string|null $uuid
     * @return string
     * @throws QueueDoesNotExistException
     */
    public function publish(string $queue, string $eventName, array $data, string $uuid = null): string
    {

        if (!array_key_exists($queue, $this->queueUrlMapping)) {
            throw new QueueDoesNotExistException('The queue mapping for ' . $queue . ' does not exist');
        }

        if ($uuid === null) {
            $uuid = $this->uuidFactory->uuid4()->toString();
        }

        $messageBody = $this->messageEncoder->encode(
            new Message(
                $uuid,
                $eventName,
                $data
            )
        );

        $this->client->sendMessage(
            [
            'DelaySeconds' => 0,
            'MessageAttributes' => [],
            'MessageBody' => $messageBody,
            'QueueUrl' => $this->queueUrlMapping[$queue]
            ]
        );

        return $uuid;
    }
}
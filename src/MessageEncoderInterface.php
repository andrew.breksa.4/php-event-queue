<?php declare(strict_types=1);


namespace EventQueue;

/**
 * Interface MessageComposerInterface
 *
 * @package EventQueue
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
interface MessageEncoderInterface
{

    /**
     * @param  Message $message
     * @return string
     */
    public function encode(Message $message): string;

    /**
     * @param  string $string
     * @return Message
     */
    public function decode(string $string, array $attributes = []): Message;

}
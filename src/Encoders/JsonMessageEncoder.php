<?php declare(strict_types=1);


namespace EventQueue\Encoders;

use EventQueue\Message;
use EventQueue\MessageEncoderInterface;

/**
 * Class JsonMessageComposer
 *
 * @package EventQueue
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class JsonMessageEncoder implements MessageEncoderInterface
{

    /**
     * @param  Message $message
     * @return string
     */
    public function encode(Message $message): string
    {
        return \Suin\Json\json_encode($message);
    }

    /**
     * @param  string $string
     * @return Message
     */
    public function decode(string $string, array $attributes = []): Message
    {
        $raw = \Suin\Json\json_decode($string, true);

        return new Message(
            $raw['uuid'],
            $raw['event'],
            $raw['data'],
            $attributes
        );
    }
}
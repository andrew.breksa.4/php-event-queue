<?php declare(strict_types=1);


namespace EventQueue\Consumers;

use EventQueue\ConsumerInterface;
use EventQueue\Exceptions\QueueDoesNotExistException;
use EventQueue\Message;
use EventQueue\MessageEncoderInterface;
use EventQueue\MessageHandlerInterface;
use Predis\ClientInterface;

/**
 * Class PredisConsumer
 *
 * @package EventQueue\Consumers
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class PredisConsumer implements ConsumerInterface
{

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var array<string, string>
     */
    protected $queueKeyMapping = [];

    /**
     * @var MessageEncoderInterface
     */
    protected $messageEncoder;

    /**
     * PredisConsumer constructor.
     *
     * @param ClientInterface         $client
     * @param MessageEncoderInterface $messageEncoder
     * @param array<string, string>   $queueKeyMapping
     */
    public function __construct(
        ClientInterface $client,
        MessageEncoderInterface $messageEncoder,
        array $queueKeyMapping = []
    ) {
        $this->client = $client;
        $this->queueKeyMapping = $queueKeyMapping;
        $this->messageEncoder = $messageEncoder;
    }


    /**
     * @param  string $queue
     * @param  string $key
     * @return $this
     */
    public function registerQueueKeyMapping(string $queue, string $key): PredisConsumer
    {
        $this->queueKeyMapping[$queue] = $key;
        return $this;
    }

    /**
     * @param  string                  $queue
     * @param  MessageHandlerInterface $messageHandler
     * @return bool|null
     * @throws QueueDoesNotExistException
     */
    public function consume(string $queue, MessageHandlerInterface $messageHandler): ?bool
    {
        if (!array_key_exists($queue, $this->queueKeyMapping)) {
            throw new QueueDoesNotExistException('The queue mapping for ' . $queue . ' does not exist');
        }
        /**
         * @var int|string $messageString
         */
        $messageString = $this->client->lpop($this->queueKeyMapping[$queue]);
        if (!is_string($messageString)) {
            return null;
        }

        $message = $this->messageEncoder->decode($messageString);

        return $messageHandler->handle($message);
    }

    /**
     * Redis doesn't support ACKs, you'll need to re-queue the message in this case
     *
     * @param Message $message
     */
    public function ack(Message $message): void
    {
        return;
    }
}
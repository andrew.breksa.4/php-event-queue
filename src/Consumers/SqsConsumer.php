<?php declare(strict_types=1);


namespace EventQueue\Consumers;


use Aws\Sqs\SqsClient;
use EventQueue\ConsumerInterface;
use EventQueue\Exceptions\QueueDoesNotExistException;
use EventQueue\Message;
use EventQueue\MessageEncoderInterface;
use EventQueue\MessageHandlerInterface;

/**
 * Class SqsConsumer
 *
 * @package EventQueue\Consumers
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class SqsConsumer implements ConsumerInterface
{

    /**
     * @var SqsClient
     */
    protected $client;

    /**
     * @var array<string, string>
     */
    protected $queueUrlMapping = [];

    /**
     * @var MessageEncoderInterface
     */
    protected $messageEncoder;

    /**
     * SqsConsumer constructor.
     *
     * @param SqsClient               $client
     * @param array<string, string>   $queueUrlMapping
     * @param MessageEncoderInterface $messageEncoder
     */
    public function __construct(SqsClient $client, MessageEncoderInterface $messageEncoder, array $queueUrlMapping = [])
    {
        $this->client = $client;
        $this->queueUrlMapping = $queueUrlMapping;
        $this->messageEncoder = $messageEncoder;
    }


    /**
     * @param Message $message
     */
    public function ack(Message $message): void
    {
        $this->client->deleteMessage(
            [
            'QueueUrl' => $message->getAttributes()['queueUrl'],
            'ReceiptHandle' => $message->getAttributes()['receiptHandle']
            ]
        );
    }

    /**
     * @param  string $queue
     * @param  string $url
     * @return $this
     */
    public function registerQueueKeyMapping(string $queue, string $url): SqsConsumer
    {
        $this->queueUrlMapping[$queue] = $url;
        return $this;
    }

    /**
     * @param  string                  $queue
     * @param  MessageHandlerInterface $messageHandler
     * @return bool|null
     * @throws QueueDoesNotExistException
     */
    public function consume(string $queue, MessageHandlerInterface $messageHandler, int $waitTimeSeconds = 0): ?bool
    {

        if (!array_key_exists($queue, $this->queueUrlMapping)) {
            throw new QueueDoesNotExistException('The queue mapping for ' . $queue . ' does not exist');
        }

        $result = $this->client->receiveMessage(
            array(
            'MaxNumberOfMessages' => 1,
            'MessageAttributeNames' => ['All'],
            'QueueUrl' => $this->queueUrlMapping[$queue],
            'WaitTimeSeconds' => $waitTimeSeconds,
            )
        );

        if (empty($result->get('Messages'))) {
            return null;
        }

        $messageString = $result->get('Messages')[0]['Body'];

        $message = $this->messageEncoder->decode(
            $messageString, [
            'queueUrl' => $this->queueUrlMapping[$queue],
            'receiptHandle' => $result->get('Messages')[0]['ReceiptHandle']
            ]
        );

        return $messageHandler->handle($message);
    }
}
<?php declare(strict_types=1);


namespace EventQueue;

/**
 * Interface ConsumerInterface
 *
 * @package EventQueue
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
interface ConsumerInterface
{

    /**
     * @param  string                  $queue
     * @param  MessageHandlerInterface $messageHandler
     * @return bool|null
     */
    public function consume(string $queue, MessageHandlerInterface $messageHandler): ?bool;

    /**
     * @param Message $message
     */
    public function ack(Message $message): void;

}
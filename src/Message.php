<?php declare(strict_types=1);


namespace EventQueue;


use JsonSerializable;

/**
 * Class Message
 *
 * @package EventQueue
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class Message implements JsonSerializable
{
    /**
     * @var string
     */
    protected $uuid;

    /**
     * @var string
     */
    protected $event;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Message constructor.
     *
     * @param string $uuid
     * @param string $event
     * @param array  $data
     */
    public function __construct(string $uuid, string $event, array $data, array $attributes = [])
    {
        $this->uuid = $uuid;
        $this->event = $event;
        $this->data = $data;
        $this->attributes = $attributes;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            'uuid' => $this->getUuid(),
            'event' => $this->getEvent(),
            'data' => $this->getData()
        ];
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }


}
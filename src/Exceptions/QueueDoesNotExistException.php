<?php declare(strict_types=1);


namespace EventQueue\Exceptions;


use Exception;

/**
 * Class QueueDoesNotExistException
 *
 * @package EventQueue\Exceptions
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
class QueueDoesNotExistException extends Exception
{

}
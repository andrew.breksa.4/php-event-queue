<?php declare(strict_types=1);


namespace EventQueue;

use JsonSerializable;

/**
 * Interface PublisherInterface
 *
 * @package EventQueue
 * @author  Andrew Breksa <andrew@andrewbreksa.com>
 */
interface PublisherInterface
{

    /**
     * @param  string      $queue
     * @param  string      $eventName
     * @param  array       $data
     * @param  string|null $uuid
     * @return string
     */
    public function publish(string $queue, string $eventName, array $data, string $uuid = null): string;
}
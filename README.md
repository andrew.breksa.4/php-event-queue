php-event-queue
---------------

[![pipeline status](https://gitlab.com/andrew.breksa.4/php-event-queue/badges/master/pipeline.svg)](https://gitlab.com/andrew.breksa.4/php-event-queue/-/commits/master)
[![coverage report](https://gitlab.com/andrew.breksa.4/php-event-queue/badges/master/coverage.svg)](https://gitlab.com/andrew.breksa.4/php-event-queue/-/commits/master)

_A simple event queue abstraction designed for flexibility_

# Features
- Dead-simple message publishing and consuming
- language agnostic message encoding when using the built in JSON encoder
- Designed to support pluggable brokers
- Supports pluggable message encoders (default/provided is JSON)

# Supported Brokers
- Redis
- SQS

# Usage
## Publishing an event with predis
```php
$publisher = new PredisPublisher(
    $predis,
    $encoder,
    $uuidFactory,
    [
        'low' => 'low_queue'
    ] 
);

$uuid = $publisher->publish('low', 'test_event', [
    'foo' => 'bar'
]);
```

## Consuming an event with predis
```php
$consumer = new PredisConsumer(
    $predis,
    $encoder,
    [
        'low' => 'low_queue'
    ]
);

$handler = new class implements MessageHandlerInterface {
    public function handle(Message $message): bool
    {
        // do some work here, and return true/false if the work is successful
        return true;
    }
};

// $consumer->consume will return the value passed returned by the handler
$consumer->consume('low', $handler); 
```

## Publishing an event with SQS
```php
/*
 * $queueUrl should be something like:
 * $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');
 */
$publisher = new SqsPublisher(
    $predis,
    $encoder,
    $uuidFactory,
    [
        'low' => $queueUrl
    ]
);

$uuid = $publisher->publish('low', 'test_event', [
    'foo' => 'bar'
]);
```

## Consume an event with SQS + ack
```php
/*
 * $queueUrl should be something like:
 * $queueUrl = $this->client->getQueueUrl(['QueueName' => 'low_queue'])->get('QueueUrl');
 */
$consumer = new SqsConsumer(
    $predis,
    $encoder,
    [
        'low' => $queueUrl
    ]
);

$handler = new class($consumer) implements MessageHandlerInterface {

    /**
     * @var ConsumerInterface
     */
    protected $consumer;

    /**
     *  constructor.
     * @param ConsumerInterface $consumer
     */
    public function __construct(ConsumerInterface $consumer)
    {
        $this->consumer = $consumer;
    }


    public function handle(Message $message): bool
    {
        $this->consumer->ack($message);

        return true;
    }
};

$consumer->consume('low', $handler, 10); //optional wait timeout for a message to appear
```

## Deferred Publisher 
```php
/*
 * Here we can use any implementation fo the PublisherInterface, the DeferredPublisher is just a wrapper
 */
$deferredPublisher = new DeferredPublisher(
    $publisher,
    $uuidFactory
);

$uuid = $deferredPublisher->publish('low', 'test_event', [
    'foo' => 'bar'
]);

$deferredPublisher->flush(); // here the messages are actually sent
```

# Message format
When using the built in JSON message encoder, the event looks something like:
```json
{
  "uuid": "85e7d505-4c77-47d5-8e56-c20a4b4d758a",
  "event": "test_event",
  "data": {
    "foo": "bar"
  }
}
```

# Testing
- Clone the source 
- Install dependencies via composer (`composer install`)
- Copy and update the `.env.dist` file to `.env`
- Bring up the docker-compose stack (`docker-compose up`)
- Execute `php ./vendor/bin/phpunit ./tests --coverage-text`